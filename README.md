# Leben ist Glück
## Mia, Maximilian & Sophie

---


# Fragestellung: 
### Ungerechtigkeit aufzeigen/verstehen/nachempfinden

- Ungerechtigkeit in der Gesellschaft
- Nicht jeder startet mit dem gleichen Voraussetzungen

---

# Das Endprodukt:

- Browserspiel
- interaktive Geschichte
- zufällige Ereignisse

---

# Daten und Technologien

- Editor: [Twine](https://twinery.org/) v2.4
- Sprache: [Harlowe](https://twine2.neocities.org/) v3.3

---

# Live-Demo

![](https://pad.medialepfade.net/uploads/1f1a99019c9d38b1cc44ea363.png)

---

# Screenshot

![](https://pad.medialepfade.net/uploads/1f1a99019c9d38b1cc44ea35a.png)


---

# Prozess

![](https://pad.medialepfade.net/uploads/1f1a99019c9d38b1cc44ea35e.jpg)

---

# Ausblick und offene Punkte

- Neues Format -> Prototyp übertragen
    - vielleicht Scratch, Dart/Flutter, Python
- gemeinsame Weiterentwicklung
- Veröffentlichung
    - App Store?


---

## Vielen Dank für das Zuschauen!

### Wir hoffen, es hat euch gefallen.

https://gitlab.com/jugendhackt/spiel-des-lebens
